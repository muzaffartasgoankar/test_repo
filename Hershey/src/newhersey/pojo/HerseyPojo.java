package newhersey.pojo;

public class HerseyPojo {
	
	//file_name,stage,attri,max_limits,test_method,sampling
	private String file_name = "";
	private String stage = "";
	private String attri = "";
	private String max_limits = "";
	private String test_method = "";
	private String sampling = "";
	
	
	
	public HerseyPojo(String stage) {
		this.stage = stage;
	}
	public HerseyPojo() {
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public String getStage() {
		return stage;
	}
	public void setStage(String stage) {
		this.stage = stage;
	}
	public String getAttri() {
		return attri;
	}
	public void setAttri(String attri) {
		this.attri = attri;
	}
	public String getMax_limits() {
		return max_limits;
	}
	public void setMax_limits(String max_limits) {
		this.max_limits = max_limits;
	}
	public String getTest_method() {
		return test_method;
	}
	public void setTest_method(String test_method) {
		this.test_method = test_method;
	}
	public String getSampling() {
		return sampling;
	}
	public void setSampling(String sampling) {
		this.sampling = sampling;
	}
	@Override
	public String toString() {
		return "HerseyPojo [file_name=" + file_name + ", stage=" + stage + ", attri=" + attri + ", max_limits="
				+ max_limits + ", test_method=" + test_method + ", sampling=" + sampling + "]";
	}
//	public Object ignoreStagevalue(String string) {
//		
//		return stage;
//	}
//	public Object ignoreMaxvalue(String string) {
//		
//		return max_limits;
//	}
//	public Object ignoreSamplingvalue(String string) {
//		
//		return sampling;
//	}
//	public Object ignoreTestvalue(String string) {
//		
//		return test_method;
//	}
//	public Object ignoreAttrivalue(String string) {
//		
//		return attri;
//	}
//	
	

}
