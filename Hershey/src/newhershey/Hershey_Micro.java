package newhershey;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import newhersey.pojo.HerseyPojo;

import java.util.Iterator;;

public class Hershey_Micro {

	public static void main(String[] args) throws IOException, SQLException {
		Connection con = null;
		ResultSet rs = null;
		// ResultSet rs2 = null;
		PreparedStatement ps = null;
		DbUtil dbUtil = new DbUtil();
		
		con = dbUtil.getPostGresConnection();
		Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		List<Integer> column = new ArrayList<>();
		File path = new File("E:\\hershey_file_name.txt");
		BufferedReader br = new BufferedReader(new FileReader(path));
		String file_name = "";
		
		List<HerseyPojo> pojoList = new ArrayList<>();
		HerseyPojo pojo = null;
		File ignore = new File("E:\\hershey_micro_ignore.txt");
		BufferedWriter bw = new BufferedWriter(new FileWriter(ignore));
		String previousStage = "";
		try {
			String sql2 = "INSERT INTO datadump_hershey_muzzu_file_micro(file_name,stage,attri,max_limits,test_method,sampling) VALUES(?,?,?,?,?,?)";
			while ((file_name = br.readLine()) != null)
			//if ((file_name = br.readLine()) != null) 
			{

				file_name = file_name.replace("'", "''");
				String sql1 = "select file_name,raw_text,row_index,col_index from boa.hershey_data_dump where file_name='"
						+ file_name + "' \r\n" + "	and border_weight!='2' and row_height!='30' and row_index>(\r\n"
						+ "		select row_index from boa.hershey_data_dump\r\n" + "			where file_name='"
						+ file_name + "' and raw_text='Microbiological'\r\n" + "	)\r\n" + "	and row_index<(\r\n"
						+ "		select row_index from boa.hershey_data_dump\r\n" + "			where file_name='"
						+ file_name + "' and raw_text='WEIGHT CONTROL REQUIREMENTS'\r\n" + "	)\r\n"
						+ "	order by row_index,col_index\r\n" + "";

				String sql3 = "select distinct col_index from boa.hershey_data_dump where file_name='" + file_name
						+ "' \r\n" + "	and border_weight!='2' and row_index>(\r\n"
						+ "		select row_index from boa.hershey_data_dump\r\n" + "			where file_name='"
						+ file_name + "' and raw_text='Microbiological'\r\n" + "	)\r\n" + "	and row_index<(\r\n"
						+ "		select row_index from boa.hershey_data_dump\r\n" + "			where file_name='"
						+ file_name + "' and raw_text  like 'WEIGHT%'\r\n" + "	)\r\n"
						+ "	order by col_index\r\n" + "";

				
				
				try {
					ps = con.prepareStatement(sql2);
					System.out.println(sql1);
					rs = stmt.executeQuery(sql1);
					int readFile  = 1;
					int writeFile = 1;
					int previousRowIndex = 0;
					
					while(rs.next()) {
						
//						if (pojo.getStage().equals("Stage")
//								|| pojo.getStage().equals("Attribute")
//								|| pojo.getStage().equals("Maximum Limits Cfu/g or Cfu/ml")
//								|| pojo.getStage().equals("Test Method")
//								|| pojo.getStage().equals("Sampling"))
//
//							;
						
						String fileName = rs.getString("file_name");
						String rawText = rs.getString("raw_text");
						int rowIndex = rs.getInt("row_index");
						int colIndex = rs.getInt("col_index");
						
						if(previousRowIndex != rowIndex) {
							previousRowIndex = rowIndex;
							if(pojo != null) {
								pojoList.add(pojo);
								System.out.println(pojo);
							}
							pojo = new HerseyPojo(previousStage);
							pojo.setFile_name(fileName);
						}
						
						
						if(colIndex == 1) {
							
							//System.out.println(readFile++ + " Reading file : " + fileName);
							pojo.setFile_name(fileName);
							pojo.setStage(rawText);
							previousStage = rawText;
						}
						else if(colIndex == 2) {
							
							pojo.setAttri(rawText);
						}
						else if(colIndex == 3) {
							
							pojo.setMax_limits(rawText);
						}
						else if(colIndex == 4) {
							
							pojo.setTest_method(rawText);
						}
						else if(colIndex == 5) {
						
							pojo.setSampling(rawText);
							//System.out.println(writeFile++ + " Added file : " + fileName);
							
						}
						
						
					}
					
					ps.executeBatch();
					
					/*
					rs = stmt.executeQuery(sql3);
					if (rs.next()) {
						do {
							column.add(rs.getInt("col_index"));
						} while (rs.next());
					}
					// rs.close();

					rs = stmt.executeQuery(sql1);
					ps = con.prepareStatement(sql2);

					try {
						if (rs.next() == false) {
							bw.write(file_name + " not completed\n");
						} else {
							do {

								if (rs.getString("raw_text").equals("Stage")
										|| rs.getString("raw_text").equals("Attribute")
										|| rs.getString("raw_text").equals("Maximum Limits Cfu/g or Cfu/ml")
										|| rs.getString("raw_text").equals("Sampling")
										|| rs.getString("raw_text").equals("Test Method"))

									;
								else {
									// System.out.println(column);
									// for (int i : column) {
									//// System.err.println(i);
									// if (rs.getInt("col_index") != column.get(i)) {
									// ps.setString(i + 2, "");
									//
									// }
									// }

									if (rs.getInt("col_index") == 1) {

										List<String[]> results = new ArrayList<>();
										results.add(new String[] { rs.getString("raw_text") });
										for (String[] e : results) {
											for (int i = 0; i < e.length; i++) {
												try {

													ps.setString(1, rs.getString("file_name"));
													ps.setString(2, e[i]);

												} catch (SQLException e1) {

													e1.printStackTrace();
												}
											}
										}
									}
									if (rs.getInt("col_index") == 2) {
										List<String[]> results = new ArrayList<>();
										results.add(new String[] { rs.getString("raw_text") });
										for (String[] e : results) {
											for (int i = 0; i < e.length; i++) {
												try {

													// if (e[i].trim() == "" || e[i].isEmpty())
													// // if(rs.isBeforeFirst())
													// {
													//
													// ps.setString(3, "NULL");
													// }
													if (rs.getString("raw_text") == null) {
														ps.setString(3, "NULL");
													} else {
														ps.setString(3, e[i]);

													}
													//

												} catch (SQLException e1) {

													e1.printStackTrace();
												}

												// System.out.print(e[i] + " ");
											}
										}
									}
									if (rs.getInt("col_index") == 3) {
										List<String[]> results = new ArrayList<>();
										results.add(new String[] { rs.getString("raw_text") });
										for (String[] e : results) {
											for (int i = 0; i < e.length; i++) {
												try {

													// if (e[i].trim() == "" || e[i].isEmpty())
													// // if(rs.isBeforeFirst())
													// {
													//
													// ps.setString(4, "NULL");
													// }
													if (rs.getString("raw_text") == null) {
														ps.setString(4, "NULL");
													} else {
														ps.setString(4, e[i]);

													}

													//
												} catch (SQLException e1) {

													e1.printStackTrace();
												}

												// System.out.print(e[i] + " ");
											}
										}
									}
									if (rs.getInt("col_index") == 4) {
										try {

											List<String[]> results = new ArrayList<>();
											results.add(new String[] { rs.getString("raw_text") });
											for (String[] e : results) {
												for (int i = 0; i < e.length; i++) {
													try {

														// if (e[i].trim() == "" || e[i].isEmpty())
														// // if(rs.isBeforeFirst())
														// {
														//
														// ps.setString(5, "NULL");
														// }

														if (rs.getString("raw_text") == null) {
															ps.setString(5, "NULL");
														} else {
															ps.setString(5, e[i]);

														}
														//

													} catch (SQLException e1) {

														e1.printStackTrace();
													}

												}
											}

										} catch (Exception e) {
											e.printStackTrace();
										}

									}
									if (rs.getInt("col_index") == 5) {

										List<String[]> results = new ArrayList<>();
										results.add(new String[] { rs.getString("raw_text") });
										for (String[] e : results) {
											for (int i = 0; i < e.length; i++) {
												try {

													// if (e[i].trim() == "" || e[i].isEmpty())
													// // if(rs.isBeforeFirst())
													// {
													//
													// ps.setString(6, "NULL");
													// }

													if (rs.getString("raw_text") == null) {
														ps.setString(6, "NULL");
													} else {
														ps.setString(6, e[i]);

													}

												} catch (SQLException e1) {

													e1.printStackTrace();
												}

											}
										}
										// System.out.println();
										ps.addBatch();
										ps.executeBatch();
									}

								}

							} while (rs.next());
							// column.clear();

						}

					} catch (Exception e1) {
						bw.write(file_name + " not completed\n");
						e1.printStackTrace();
					}
				*/} catch (Exception e) {
					bw.write(file_name + " not completed\n");
					e.printStackTrace();
				}
			}
			
			try {
				PreparedStatement psmt = con.prepareStatement(sql2);
				for(HerseyPojo temp : pojoList) {
					psmt.setString(1, temp.getFile_name());
					psmt.setString(2, temp.getStage());
					psmt.setString(3, temp.getAttri());
					psmt.setString(4, temp.getMax_limits());
					psmt.setString(5, temp.getTest_method());
					psmt.setString(6, temp.getSampling());
					psmt.addBatch();
				}
				psmt.executeBatch();
			} catch(Exception e) {
				e.printStackTrace();
			}
			con.close();
		} catch (Exception e2) {
			bw.write(file_name + " not completed\n");
			e2.printStackTrace();
		}
		bw.close();
	}

}
