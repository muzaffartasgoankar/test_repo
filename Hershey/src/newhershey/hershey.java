package newhershey;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Iterator;;

public class hershey {

	public static void main(String[] args) throws IOException, SQLException {
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		DbUtil dbUtil = new DbUtil();
		con = dbUtil.getPostGresConnection();
		Statement stmt = con.createStatement();
		List<Integer> column = new ArrayList<>();
		File path = new File("E:\\hershey_file_name.txt");
		BufferedReader br = new BufferedReader(new FileReader(path));
		String file_name = "";

		File ignore = new File("E:\\hershey_ignoreFile.txt");
		BufferedWriter bw = new BufferedWriter(new FileWriter(ignore));
		try {

			while ((file_name = br.readLine()) != null) {
				file_name = file_name.replace("'", "''");
				String sql1 = "select * from boa.hershey_data_dump where file_name='" + file_name + "' \r\n"
						+ "	and border_weight!='2' and row_index>(\r\n"
						+ "		select row_index from boa.hershey_data_dump\r\n" + "			where file_name='"
						+ file_name + "' and raw_text='Analytical'\r\n" + "	)\r\n" + "	and row_index<(\r\n"
						+ "		select row_index from boa.hershey_data_dump\r\n" + "			where file_name='"
						+ file_name + "' and raw_text='Microbiological' or '3. MICROBIOLOG' or 'CHANGE LOG' or 'Photos'\r\n" + "	)\r\n"
						+ "	order by row_index,col_index\r\n" + "";

				// String sql3 = "select distinct col_index from boa.hershey_data_dump where
				// file_name='" + file_name
				// + "' \r\n" + " and border_weight!='2' and row_index>(\r\n" + " select
				// row_index from boa.hershey_data_dump\r\n"
				// + " where file_name='" + file_name + "' and raw_text='Analytical'\r\n" + "
				// )\r\n"
				// + " and row_index<(\r\n" + " select row_index from boa.hershey_data_dump\r\n"
				// + " where file_name='" + file_name + "' and raw_text='Microbiological'\r\n" +
				// " )\r\n"
				// + " order by col_index\r\n" + "";

				String sql2 = "INSERT INTO datadump_hershey_muzzu_file(file_name,stage,attri,target,tolerance,sampling,test_method) VALUES(?,?,?,?,?,?,?)";

				// try {
				// rs = stmt.executeQuery(sql3);
				// if (rs.next()) {
				// do {
				// column.add(rs.getInt("col_index"));
				// } while (rs.next());
				// }
				// } catch (Exception e) {
				// bw.write(file_name + " not completed\n");
				// System.out.println("File is not there in Db");
				// continue;
				// }
				// Iterator<Integer> iterator = column.iterator();
				// while (iterator.hasNext()) {
				//
				// int temp = iterator.next();
				try {
					rs = stmt.executeQuery(sql1);
					ps = con.prepareStatement(sql2);

					try {
						if (rs.next()) {
							do {
								// String regex = "[a-z] [0-9] [a-z] [0-9]";
								// Pattern p = Pattern.compile(regex);
								// Matcher matcher = p.matcher(rs.getString("raw_text"));
								// String[] arrSplit_3 = file_name.substring(0,file_name.length()-5)
								if (rs.getString("raw_text").equals("PQS COMFG TECHNICAL")
										// || rs.getString("raw_text").equals("PQS COMFG INTERMEDIATE")
										// || rs.getString("raw_text").equals(file_name.substring(0, file_name.length()
										// - 5))
										// || rs.getString("raw_text").equals("flavors")
										// || rs.getString("raw_text").equals("App/Ch, Jolly Rancher")
										// || rs.getString("raw_text").equals("Paste, Milk Chocolate, Krackel")
										//// || matcher.find()
										// || rs.getString("raw_text").equals("Company Confidential")
										// || rs.getString("raw_text").equals("The Hershey Company")
										|| rs.getString("raw_text").equals("Stage")
										|| rs.getString("raw_text").equals("Attribute")
										|| rs.getString("raw_text").equals("Target")
										|| rs.getString("raw_text").equals("Tolerance")
										|| rs.getString("raw_text").equals("Sampling")
										|| rs.getString("raw_text").equals("Test Method"))

									;
								else {

									if ((rs.getInt("col_index") % 6) != 1) {
										System.out.print("\t");
									}
									if (rs.getInt("col_index") == 1) {

										// if(rs.getString("raw_text")==null)
										// {
										// ps.setString(1,"");
										// }

										List<String[]> results = new ArrayList<>();
										results.add(new String[] { rs.getString("raw_text") });
										for (String[] e : results) {
											for (int i = 0; i < e.length; i++) {
												try {
													ps.setString(1, rs.getString("file_name"));
													ps.setString(2, e[i]);

												} catch (SQLException e1) {

													e1.printStackTrace();
												}

												// System.out.print(e[i] + " ");
											}
										}
									}
									if (rs.getInt("col_index") == 2) {
										List<String[]> results = new ArrayList<>();
										results.add(new String[] { rs.getString("raw_text") });
										for (String[] e : results) {
											for (int i = 0; i < e.length; i++) {
												try {

													if (rs.getString("raw_text") == null) {
														ps.setString(3, "NULL");
													} else {
														ps.setString(3, e[i]);

													}
													// ps.setString(3, e[i]);

												} catch (SQLException e1) {

													e1.printStackTrace();
												}

												// System.out.print(e[i] + " ");
											}
										}
									}
									if (rs.getInt("col_index") == 3) {
										List<String[]> results = new ArrayList<>();
										results.add(new String[] { rs.getString("raw_text") });
										for (String[] e : results) {
											for (int i = 0; i < e.length; i++) {
												try {

													if (rs.getString("raw_text") == null) {
														ps.setString(4, "NULL");
													} else {
														ps.setString(4, e[i]);

													}

													// ps.setString(4, e[i]);
												} catch (SQLException e1) {

													e1.printStackTrace();
												}

												// System.out.print(e[i] + " ");
											}
										}
									}
									if (rs.getInt("col_index") == 4) {

										List<String[]> results = new ArrayList<>();
										results.add(new String[] { rs.getString("raw_text") });
										for (String[] e : results) {
											for (int i = 0; i < e.length; i++) {
												try {

													if (rs.getString("raw_text") == null) {
														ps.setString(5, "NULL");
													} else {
														ps.setString(5, e[i]);

													}

													// ps.setString(5, e[i]);
												} catch (SQLException e1) {

													e1.printStackTrace();
												}

												// System.out.print(e[i] + " ");
											}
										}
									}
									if (rs.getInt("col_index") == 5) {
										List<String[]> results = new ArrayList<>();
										results.add(new String[] { rs.getString("raw_text") });
										for (String[] e : results) {
											for (int i = 0; i < e.length; i++) {
												try {

													if (rs.getString("raw_text") == null) {
														ps.setString(6, "NULL");
													} else {
														ps.setString(6, e[i]);

													}

													// ps.setString(6, e[i]);
												} catch (SQLException e1) {

													e1.printStackTrace();
												}

												// System.out.print(e[i] + " ");
											}
										}
									}
									if (rs.getInt("col_index") == 6) {
										List<String[]> results = new ArrayList<>();
										results.add(new String[] { rs.getString("raw_text") });
										for (String[] e : results) {
											for (int i = 0; i < e.length; i++) {
												try {

													if (rs.getString("raw_text") == null) {
														ps.setString(7, "NULL");
													} else {
														ps.setString(7, e[i]);

													}

													// ps.setString(7, e[i]);
												} catch (SQLException e1) {

													e1.printStackTrace();
												}

												// System.out.print(e[i] + " ");
											}
										}
										System.out.println();
										ps.addBatch();
										ps.executeBatch();
										// System.out.println("Batch executed successfully");

									}
								}

							} while (rs.next());
						}
					} catch (Exception e1) {
						bw.write(file_name + " not completed\n");
						// bw.write(file_name + " not completed\n");
						// System.out.println("File is not there in Db");
						e1.printStackTrace();
					}
				}
				// }
				catch (Exception e) {
					bw.write(file_name + " not completed\n");
					e.printStackTrace();
				}
			}

		} catch (Exception e2) {
			bw.write(file_name + " not completed\n");
			e2.printStackTrace();
		}
		bw.close();
	}
	// bw.close();

}
